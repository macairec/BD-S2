﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace BDProject
{
    public partial class Form1 : Form
    {
        OleDbConnection dbConnection;
        int code_Abonné_connecté = -1;
        public Form1()
        {
            InitializeComponent();
            Init();
            TrouveMusiciens("%");
        }
        private void Init()
        {
            string ChaineBd = "Provider=SQLOLEDB;Data Source=INFO-SIMPLET;Initial Catalog=Musique;Uid=ETD; Pwd=ETD";
            dbConnection = new OleDbConnection(ChaineBd);
            dbConnection.Open();
        }
        private void TrouveMusiciens(string initiale)
        {
            string sql = "Select Code_Musicien, Nom_Musicien, Prénom_Musicien FROM Musicien "
                + " WHERE Nom_Musicien Like ? ORDER BY Nom_Musicien";
            OleDbCommand cmd = new OleDbCommand(sql, dbConnection);
            cmd.Parameters.Add("init", OleDbType.VarChar).Value = initiale;
            OleDbDataReader reader = cmd.ExecuteReader();
            List<Musicien> musiciens = new List<Musicien>();
            while (reader.Read())
            {
                Musicien musicien = new Musicien();
                musicien.Code_Musicien = reader.GetInt32(0);
                musicien.Nom_Musicien = reader.GetString(1);
                if (!reader.IsDBNull(2))
                    musicien.Prénom_Musicien = reader.GetString(2);
                else
                    musicien.Prénom_Musicien = "Inconnu";
                musiciens.Add(musicien);
                listBox1.Items.Add(musicien);
            }
            reader.Close();
        }

        private void ChargerOeuvres()
        {
            bool vide = true;
            listBox2.Items.Clear();
            Musicien musicien = (Musicien)listBox1.SelectedItem;
            if (musicien == null)
                return;
            int id = musicien.Code_Musicien;
            string sql = "Select Titre_Oeuvre FROM Oeuvre "
                + " INNER JOIN Composer ON Oeuvre.Code_Oeuvre = Composer.Code_Oeuvre "
                + " WHERE Code_Musicien Like " + id.ToString();
            sql += "ORDER BY Titre_Oeuvre";
            OleDbCommand cmd = new OleDbCommand(sql, dbConnection);
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Oeuvre oeuvre = new Oeuvre(reader.GetString(0), musicien);
                listBox2.Items.Add(oeuvre);
                vide = false;
            }

            if (vide)
            {
                listBox2.Items.Add("Ce musicien n'a rien composé.");
            }
            reader.Close();
        }

        private void ChargerAlbums()
        {
            try
            {
                bool vide = true;
                listBox3.Items.Clear();
                if (listBox2.SelectedItem.GetType() == "".GetType())
                    return;
                Oeuvre oeuvre = (Oeuvre)listBox2.SelectedItem;
                if (oeuvre == null)
                    return;
                Musicien musician = oeuvre.Mu;
                string sql = "SELECT DISTINCT Titre_Album, Code_Album FROM Album ";
                /*
                    + " INNER JOIN Disque ON Album.Code_Album = Disque.Code_Album "
                    + " INNER JOIN Composition_Disque ON Composition_Disque.Code_Disque = Disque.Code_Disque "
                    + " INNER JOIN Enregistrement ON Enregistrement.Code_Enregistrement = Composition_Disque.Code_Enregistrement "
                    + " INNER JOIN Composition ON Composition.Code_Composition = Enregistrement.Code_Composition "
                    + " INNER JOIN Composition_Oeuvre ON Composition.Code_Composition = Enregistrement.Code_Composition "
                    + " INNER JOIN Oeuvre ON Oeuvre.Code_Oeuvre = Composition_Oeuvre.Code_Oeuvre "
                    + " INNER JOIN Composer ON Composer.Code_Oeuvre = Oeuvre.Code_Oeuvre "
                    + " INNER JOIN Musicien ON Composer.Code_Musicien = Musicien.Code_Musicien ";
                    + " WHERE Musicien.Code_Musicien LIKE " + musician.Code_Musicien + " AND " + " Oeuvre.Code_Oeuvre LIKE " + oeuvre.Code_Oeuvre;
                */
                sql += " ORDER BY Titre_Album";
                OleDbCommand cmd = new OleDbCommand(sql, dbConnection);
                OleDbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Album album = new Album(reader.GetString(0), reader.GetInt32(1));
                    listBox3.Items.Add(album);
                    vide = false;
                }

                if (vide)
                {
                    listBox3.Items.Add("Cette oeuvre n'a pas d'album.");
                }
                reader.Close();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {}

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChargerOeuvres();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChargerAlbums();
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            TrouveMusiciens(textBox1.Text + "%");
        }

        private void button3_Click(object sender, EventArgs e) // Connexion
        {
            string login = textBox_login.Text;
            string psw = textBox_password.Text;
            if (login != "" && psw != "")
            {
                string sql = "SELECT Code_Abonné, Nom_Abonné, Prénom_Abonné FROM Abonné "
                    + " WHERE Login = '" + login + "' AND Password = '" + psw + "'";
                OleDbCommand cmd = new OleDbCommand(sql, dbConnection);
                OleDbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    code_Abonné_connecté = reader.GetInt32(0);
                    string abo_nom = reader.GetString(1);
                    string abo_prenom = reader.GetString(2);
                    MessageBox.Show("Vous êtes connecté sur le compte de " + abo_nom + " " + abo_prenom, "Connexion réussie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox3.SelectedItem != null)
            {
                Album album = (Album)listBox3.SelectedItem;

                string sql = "SELECT DISTINCT Abonné.Code_Abonné, Prénom_Abonné, Nom_Abonné FROM Abonné"
                    + " JOIN Emprunter ON Abonné.Code_Abonné = Emprunter.Code_Abonné"
                    + " JOIN Album ON Emprunter.Code_Album = Album.Code_Album"
                    + " WHERE Album.Code_Album LIKE '" + album.Code_Album.ToString() + "'"; 
                OleDbCommand cmd = new OleDbCommand(sql, dbConnection);
                OleDbDataReader reader = cmd.ExecuteReader();
                reader.Read();
                if(!reader.HasRows)
                {
                    #region Emprunter
                    string sql_emprunter = "INSERT INTO Emprunter(Code_Album, Code_Abonné)"
                    + " VALUES(" + album.Code_Album.ToString() + ", " + code_Abonné_connecté.ToString() + ")";
                    OleDbCommand cmd_emprunter = new OleDbCommand(sql_emprunter, dbConnection);
                    OleDbDataReader reader_emprunter = cmd_emprunter.ExecuteReader();
                    reader_emprunter.Read();
                    MessageBox.Show("Vous avez emprunté " + album.Titre_Album, "Emprunt réussie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    #endregion
                }
                else
                {
                    int code_réserveur = reader.GetInt32(1);
                    if(code_réserveur == code_Abonné_connecté)
                    {
                        #region Libérer
                        string sql_libérer = "DELETE FROM Emprunter WHERE Code_Album = " + album.Code_Album.ToString() + "'";
                        OleDbCommand cmd_libérer = new OleDbCommand(sql_libérer, dbConnection);
                        cmd_libérer.ExecuteNonQuery();
                        MessageBox.Show("L'album " + album.Titre_Album + " est libre de nouveau", "Libération réussie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        #endregion
                    }
                    else
                    {
                        #region Déjà réservée -> impossible
                        MessageBox.Show("L'album " + album.Titre_Album + " est déjà pris", "Opération impossible", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        #endregion
                    }
                }
                reader.Close();
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        { 
            Inscription ins = new Inscription();
            ins.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            code_Abonné_connecté = -1;
            MessageBox.Show("Vous êtes déconnecté");
        }
    }
}
