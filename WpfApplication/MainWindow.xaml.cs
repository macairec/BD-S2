﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Navigation;

namespace WpfApplication
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        public List<Musicien> Compositeurs { get; set; }
        public List<Album> Albums { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel();

            MusiqueEntities m = new MusiqueEntities();
            Compositeurs = (m.Musicien.Where(c => c.Oeuvre.Count() > 0).ToList());
            Albums = (m.Album.OrderBy(c => c.Titre_Album).ToList());
        }

        private void listeAlbums_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Ne marche pas car DataContext n'est pas accessible par cette fonction
            //DataContext.album_actuel = listeAlbums.SelectedItem;
        }
    }

    
    public class MainViewModel
    {
        public List<Musicien> Compositeurs { get; set; }
        public List<Album> Albums { get; set; }

        public Album DataContext;

        public MainViewModel()
        {
            MusiqueEntities m = new MusiqueEntities();

            Compositeurs = (m.Musicien.Where(c => c.Oeuvre.Count() > 0).ToList());
            Albums = (m.Album.OrderBy(c => c.Titre_Album).ToList());
            //m.Emprunter.Add();
            //m.SaveChanges();
        }
    }
}
