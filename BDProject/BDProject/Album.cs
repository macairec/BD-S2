﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace BDProject
{
    class Album
    {
        int code_Album;
        string titre_Album;
        public Album(string titre, int code)
        {
            titre_Album = titre;
            code_Album = code;
        }
        public int Code_Album
        {
            get { return code_Album; }
            set { code_Album = value; }
        }
        public string Titre_Album
        {
            get { return titre_Album; }
            set { titre_Album = value; }
        }
        public override string ToString()
        {
            return titre_Album;
        }
    }
}
