﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace BDProject
{
    class Abonné
    {
        int code_abonné;
        string nom_abonné;
        string prénom_abonné;
        string login;
        string password;

        public int Code_Abonné
        {
            get { return code_abonné; }
            set { code_abonné = value; }
        }
        public string Nom_Abonné
        {
            get { return nom_abonné; }
            set { nom_abonné = value; }
        }
        public string Prénom_Abonné
        {
            get { return prénom_abonné; }
            set { prénom_abonné = value; }
        }
        public string Login
        {
            get { return login; }
            set { login = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        public override string ToString()
        {
            return nom_abonné + " " + prénom_abonné;
        }
    }
}
