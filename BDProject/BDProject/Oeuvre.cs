﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDProject
{
    class Oeuvre
    {

        int code_Oeuvre;
        string titre_Oeuvre;
        int annee_Oeuvre;
        Musicien mu;

        public Oeuvre(string titre, Musicien me)
        {
            titre_Oeuvre = titre;
            mu = me;
        }

        public int Code_Oeuvre
        {
            get { return code_Oeuvre; }
            set { code_Oeuvre = value; }
        }
        public Musicien Mu
        {
            get { return mu; }
            set { mu = value; }
        }

        public string Titre_Oeuvre
        {
            get { return titre_Oeuvre; }
            set { titre_Oeuvre = value; }
        }

        public int Annee_Oeuvre
        {
            get { return annee_Oeuvre; }
            set { annee_Oeuvre = value; }
        }
        public override string ToString()
        {
            return titre_Oeuvre;
        }
    }
}
