﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace WpfApplication
{
    class ImageConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter,
        System.Globalization.CultureInfo culture)
        {
            if (value != null && value is byte[])
            {
                MemoryStream stream = new MemoryStream((byte[])value);
                BitmapImage image = new BitmapImage();
                image.BeginInit(); image.StreamSource = stream; image.EndInit();
                return image;
            }
            if (value == null)
            {
                BitmapImage image = new BitmapImage();
                return image;
            }
            return null;
        }
        object IValueConverter.ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new Exception("Pas encore implementé");
        }
    }
}
